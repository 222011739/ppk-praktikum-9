> Nama : Muhammad Naufal Faishal            
Kelas / Absen : 3SI3 / 27

## Laporan Praktikum 9

1. Membuka Aplikasi untuk pertama kali
        >![bukaAplikasi](/images/bukaAplikasi.png "Membuka Aplikasi")
1. Menutup Aplikasi
        >![keluarAplikasi](/images/keluarAplikasi.png "Menutup Aplikasi")
1. Membuka kembali Aplikasi
        >![bukaKembaliAplikasi](/images/bukaKembaliAplikasi.png "Membuka Kembali Aplikasi")
1. Mematikan Aplikasi serta dihapus dari task manager
        >![hapusAplikasi](/images/hapusAplikasi.png "Mematikan Aplikasi")


# Penjelasan

## onCreate

State onCreate berjalan ketika kita aplikasi baru saja dibuka. 

## onStart

State onStart akan berjalan ketika user membuka aplikasi, baik itu baru saja membuka atau setelah keluar dari aplikasi. Biasanya state onStart diiringi dengan state onResume.

## onResume

State onResume akan berjalan ketika user membuka aplikasi, baik itu baru saja membuka atau setelah keluar dari aplikasi. Biasanya state onResume muncul setelah state onStart. Kedua state ini selalu muncul bersamaan.

## onPause

State onPause akan berjalan ketika user keluar dari aplikasi tanpa menghapusnya pada task manager. Biasanya state ini akan berjalan ketika user memencet tombol home pada gawai yang digunakan. Biasanya state onPause ini diiringi dengan state onStop.

## onStop

Sama seperti state onPause, State onStop akan berjalan berjalan ketika user keluar dari aplikasi tanpa menghapusnya pada task manager. Biasanya state ini akan berjalan ketika user memencet tombol home pada gawai yang digunakan. State onStop akan selalu muncul setelah state onPause.

## onDestroy

State onDestroy akan aktif ketika user keluar dari aplikasi dan menghapus aplikasi tersebut pada task manager. Untuk beberapa aplikasi, state ini akan dieksekusi ketika user sedang berada pada homepage aplikasi dan memencet tombol kembali. State ini akan muncul setelah onPause dan onStop dieksekusi.

## onRestart

State onRestart akan aktif ketika user membuka kembali aplikasi yang telah ditutup sebelumnya tanpa penghapusan task aplikasi pada task manager. State onRestart akan diiringi oleh onStart lalu onResume.